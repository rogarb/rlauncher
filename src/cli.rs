//! App module: implementation of the command line options
//!
use anyhow::{anyhow, bail};
use clap::Parser;
use std::process::Command;

#[derive(Parser)]
pub struct Cli {
    #[clap(short, long = "entry", required = true, value_parser = parse_entry)]
    pub entries: Vec<Entry>,
}

#[derive(Clone, Debug)]
pub struct Entry {
    pub name: String,
    pub command: String,
}

impl Cli {
    pub fn exec(&self, name: &str) -> anyhow::Result<()> {
        for entry in self.entries.iter() {
            if entry.name.as_str() == name {
                return run_command(entry.command.as_str());
            }
        }
        bail!("{} not found in entries", name)
    }
}

fn parse_entry(input: &str) -> anyhow::Result<Entry> {
    //println!("DBG: parsing input \"{input}\"");
    let res = Entry::try_from(input);
    //println!("DBG: got {res:?}");
    res
}

fn run_command(string: &str) -> anyhow::Result<()> {
    let mut iter = string.split(' ');
    match iter.clone().count() {
        0 => bail!("Cannot build a command with an empty string"),
        1 => Command::new(string)
            .spawn()
            .map(|_| ())
            .map_err(|e| anyhow!("{e}")),
        _ => {
            let name = iter.next().unwrap();
            let mut args = Vec::new();
            for arg in iter {
                args.push(arg);
            }
            Command::new(name)
                .args(args)
                .spawn()
                .map(|_| ())
                .map_err(|e| anyhow!("{e}"))
        }
    }
}

impl TryFrom<&str> for Entry {
    type Error = anyhow::Error;
    fn try_from(entry: &str) -> Result<Self, Self::Error> {
        let mut iter = entry.split(':');
        if iter.clone().count() == 2 {
            let name = iter.next().unwrap().into();
            let command = iter.next().unwrap().into();
            Ok(Self { name, command })
        } else {
            bail!("Malformed input")
        }
    }
}
