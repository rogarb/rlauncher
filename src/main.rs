use anyhow::Result;

mod cli;
mod ui;

fn main() -> Result<()> {
    ui::Ui::run()
}
