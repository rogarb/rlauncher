//! The implementation of the user interface
//!
use crate::cli::Cli;
use anyhow::anyhow;
use clap::Parser;
use fltk::{
    app,
    button::Button,
    enums::{Align, Event, Key},
    frame::Frame,
    input::Input,
    menu,
    prelude::*,
    window::Window,
};

pub struct Ui {}

impl Ui {
    pub fn run() -> anyhow::Result<()> {
        Self::run_helper().map_err(|e| anyhow!("{e}"))
    }

    fn run_helper() -> Result<(), FltkError> {
        let cli_args = Cli::parse();
        let padding = 5;
        let app = app::App::default();
        app::set_visible_focus(false);
        let mut welcome_msg = String::from(
        "Select a program to launch in the menu using the mouse or <Up> and <Down> keys and click 'Launch' or press <Enter>. Press <ESC> or <q> to exit",
    );
        let mut wind = Window::new(100, 100, 400, 300, "rlauncher").center_screen();
        let mut frame = Frame::new(0, 0, 400, 50, "");
        frame.set_label(&welcome_msg);
        if frame.width() < frame.measure_label().0 {
            // println!("DBG: labell too wide!!");
            let numlines = frame.measure_label().0 / frame.width() + 1;
            let linewidth = welcome_msg.chars().count() / numlines as usize;
            // println!("DBG: numlines = {numlines}");
            // println!("DBG: linewidth = {linewidth}");
            let mut v = Vec::new();
            let mut tmp = String::new();
            loop {
                if welcome_msg.len() <= linewidth {
                    break;
                }
                let at = if welcome_msg.chars().nth(linewidth).unwrap() != ' ' {
                    // unwrap can't panic as linewidth is checked above
                    let mut splitat = linewidth;
                    while splitat > 0 {
                        splitat -= 1;
                        if welcome_msg.chars().nth(splitat).unwrap() == ' ' {
                            break;
                        }
                    }
                    if splitat == 0 {
                        splitat = linewidth;
                        while splitat < tmp.len() - 1 {
                            splitat += 1;
                            if welcome_msg.chars().nth(splitat).unwrap() == ' ' {
                                break;
                            }
                        }
                    }
                    splitat
                } else {
                    linewidth
                };
                tmp = welcome_msg.chars().take(at).collect::<String>();
                v.push(tmp.clone());
                welcome_msg = welcome_msg.chars().skip(at).collect::<String>();
            }
            v.push(welcome_msg);
            // println!("DBG: {v:?}");
            welcome_msg = v.into_iter().map(|l| format!("{l}\n")).collect::<String>();
            frame.set_label(&welcome_msg);
        }
        if frame.height() < frame.measure_label().1 {
            frame.set_size(frame.width(), frame.measure_label().1 + 2 * padding);
        }
        let mut choice = menu::Choice::new(0, 50, 400, 30, "Browser").below_of(&frame, padding);
        let max_entries = cli_args.entries.len() as i32;
        let msg = Frame::new(0, 150, 400, 50, "").below_of(&choice, padding);
        let mut but = Button::new(0, 200, 400, 40, "Launch")
            .below_of(&msg, padding)
            .with_align(Align::Center);
        let mut inp = Input::new(50, 10, 100, 30, None);
        inp.hide();
        for c in cli_args.entries.iter() {
            choice.add_choice(c.name.as_str());
        }
        if !cli_args.entries.is_empty() {
            choice.set_value(0);
        }
        wind.end();
        wind.show();
        let mut msg_clone = msg;
        let choice_clone = choice.clone();
        but.set_callback(move |_| {
            msg_clone.set_label(
                choice_clone
                    .choice()
                    .map(|c| format!("Launching {c}"))
                    .as_deref()
                    .unwrap_or("Select an item in the list"),
            );
            if let Some(choice) = choice_clone.choice() {
                match cli_args.exec(&choice) {
                    Ok(_) => app.quit(),
                    Err(e) => msg_clone.set_label(format!("Error: {e}").as_str()),
                }
            }
        });
        wind.handle(move |_, ev| match ev {
            Event::KeyDown => match app::event_key() {
                Key::Enter => {
                    but.do_callback();
                    true
                }
                k if k == Key::from_char('q') => {
                    app.quit();
                    true
                }
                k if k == Key::Down => {
                    let mut i = choice.value();
                    if i != -1 && i < max_entries {
                        i += 1;
                    }
                    choice.set_value(i)
                }
                k if k == Key::Up => {
                    let mut i = choice.value();
                    if i > 0 {
                        i -= 1;
                    }
                    choice.set_value(i)
                }
                _ => false,
            },
            _ => false,
        });
        app.run()
    }
}
