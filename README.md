rlauncher
=========

A customizable graphical application launcher.

Usage:
------

See `rlauncher --help`.

Installation:
-------------
`$ cargo install --git https://framagit.org/rogarb/rlauncher`

